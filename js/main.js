// Ethan Voting
// Version 1.1
//
// **** Do not use this file - it's out of date ****
//
// Copyright 2015 Ethan Nguyen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

$(document).ready(function(){
  $("#pollidform").show();
  $("#jsblurb").hide();
  $("#back").button('disable');
  $("#pollbutton").click(function(e){
    var pollid = $("#pollid").val();
    if (pollid != "") {
      initui(pollid);
      e.preventDefault();
    }
  });
  $("#polllbutton").click(function(e){
    var choice = $("input[name^=vote]:checked").attr("id");
    var choice = choice.replace("vote", "");
    var choice = parseInt(choice);

    var choiceval = $("input[name^=vote]:checked").val();

    var pollid = $("input[name^=vote]:checked").attr("name");
    var pollid = pollid.replace("vote", "");
    var pollid = parseInt(pollid);

    $("#conf p").html("Are you sure you want to choose '" + choiceval + "' as your vote? You cannot change your vote.");
    $("#conf").dialog({
      modal: true,
      dialogClass: "no-close",
      buttons: {
        "Yes": function(){
          registervote(pollid, choice);
          $(this).dialog("close");
        },
        "No": function(){
          $(this).dialog("close");
        }
      }
    });

    e.preventDefault();
  });
});

function initui(pollid) {
  $.post("ajax/server.php", {action: "1", pollid: pollid}, function(data){
    var array = $.parseJSON(data);
    if (typeof array.errorcode == "undefined") {
      $("h2").text(array.title);
      $("#pollidform").css("display", "none");
      $("#pollform").css("display", "inline");
      $("#instructions").html(array.instructions);
      $("#choices").css("display", "inline");
      array.choices = $.parseJSON(array.choices);
      var items = "";
      $.each(array.choices, function(key, value){
        items = items + "<input type='radio' name='vote"+ pollid +"'  id='vote"+ key +"' value='"+ value +"'> <label for='vote"+key+"'>" + value + "</label><br>";
      });
      $(".items").html(items);
    }
    else if (array.errorcode == "1") {
      $("#error p").html(array.description +  ", please try again.");
      $("#error").dialog({
        modal: true
      });
    }
    else {
      $("#error p").html(array.description);
      $("#error").dialog({
        modal: true
      });
    }
  });
}

function registervote(pollid, votechoice) {
  $.post("ajax/server.php", {action: "2", pollid: pollid, votechoice: votechoice}, function (data) {
    if (data == "1") {
      $("input").attr("disabled", "disabled");
      $("#polllbutton").val("Your vote has been casted");
    }
    else {
      console.log(data);
    }
  })
}
