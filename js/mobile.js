// Ethan Voting
// Version 1.1
//
// Copyright 2015 Ethan Nguyen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

$(document).bind('pageinit', function(){
  $(document).keypress(function(e){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
    	if ($("#pollidform").is(":visible") == true) {
    	  $("#pollbutton").click();
    	}
      else if ($("#pollform").is(":visible") == true) {
        $("#polllbutton").click();
      }
      e.preventDefault();
    }
  });
  $("#pollidform").show();
  $("#jsblurb").hide();
  $("#pollbutton").button('disable');
  $("#pollbutton").button('refresh');
  $("#polllbutton").button('disable');
  $("#polllbutton").button('refresh');
  $("#pollid").bind("change", function () {
    if ($("#pollid").val() != "") {
      $("#pollbutton").button('enable');
      $("#pollbutton").button('refresh');
      $("#pollbutton").val("Submit");
      $("#pollbutton").button('refresh');
    }
    else {
      $("#pollbutton").button('disable');
      $("#pollbutton").button('refresh');
    }
  })
  $("#pollbutton").bind('tap', function(e){
    $("#pollbutton").button('disable');
    $("#pollbutton").val("Loading...");
    $("#pollbutton").button('refresh');
    var pollid = $("#pollid").val();
    if (pollid != "") {
      initui(pollid);
      e.preventDefault();
    }
    else {
      // alert("Please enter a Poll ID.");
      $("#errormsg").html("Please enter a Poll ID.");
      $("#linkerror").click();
      $("#pollbutton").button('disable');
      $("#pollbutton").val("Submit");
      $("#pollbutton").button('refresh');
    }
  });
  $("#polllbutton").bind('tap', function(e){
    var choicenames = new Array();
    if ($("input[type=checkbox]:checked").length > 0) {
      var choice = new Array();
      var s = "";
      $("input[type=checkbox]:checked").each(function(){
        var id = $(this).attr("id");
        var id = id.replace("vote", "");
        choice.push(parseInt(id));
      });

      var pollid = $("input[name^=vote]:checked").first().attr("name");
      var pollid = pollid.replace("vote", "");
      var pollid = parseInt(pollid);
      choice.forEach(function (currentValue) {
        choicenames.push($("input[name^=vote]").eq(currentValue - 1).val());
      });
    }
    else {
      var choice = $("input[name^=vote]:checked").attr("id");
      var choice = choice.replace("vote", "");
      var choice = parseInt(choice);

      var choiceval = $("input[name^=vote]:checked").val();

      var pollid = $("input[name^=vote]:checked").attr("name");
      var pollid = pollid.replace("vote", "");
      var pollid = parseInt(pollid);
      choicenames.push($("input[name^=vote]").eq(choice - 1).val())
    }
    var choicelist = "";
    choicenames.forEach(function (currentValue) {
      choicelist = choicelist + "<li>" + currentValue + "</li>";
    });
    $("#confmsg").html("Are you sure you want to vote for your selected option(s)? You can only vote once. To recap, the choice(s) you have selected are: <ul>" + choicelist + "</ul>");

    $("#linkconf").click();


    e.preventDefault();
  });
  $("#confyesbtn").bind("tap", function () {
    var pollid = $("input[name^=vote]:checked").first().attr("name");
    var pollid = pollid.replace("vote", "");
    var pollid = parseInt(pollid);
    if ($("input[type=checkbox]:checked").length > 0) {
      var choice = new Array();
      var s = "";
      $("input[type=checkbox]:checked").each(function(){
        var id = $(this).attr("id");
        var id = id.replace("vote", "");
        choice.push(parseInt(id));
      });
    }
    else {
      var choice = $("input[name^=vote]:checked").attr("id");
      var choice = choice.replace("vote", "");
      var choice = parseInt(choice);

      var choiceval = $("input[name^=vote]:checked").val();
    }
    registervote(pollid, choice);
  });

});

function initui(pollid) {
  $.post("ajax/server.php", {action: "1", pollid: pollid}, function(data){
    var array = $.parseJSON(data);
    console.log(array);
    if (typeof array.errorcode == "undefined") {
      $("h2").text(array.title);
      $("#pollidform").css("display", "none");
      $("#pollform").css("display", "inline");
      $("#instructions").html(array.instructions);
      $(".choices").css("display", "inline");
      array.choices = $.parseJSON(array.choices);
      array.multiselect = $.parseJSON(array.multiselect);
      console.log(array.multiselect);
      console.log(array.multiselect.enabled);
      if (array.multiselect.enabled == 'true') {
        $("#selecttext").html("a minimum of " + array.multiselect.minselect + " and a maximum of " + array.multiselect.selectno + ":");
        var items = "";
        $.each(array.choices, function(key, value){
          items = items + "<input type='checkbox' name='vote"+ pollid +"'  id='vote"+ key +"' value='"+ value +"'> <label for='vote"+key+"'>" + value + "</label>";
        });
        $(".items").html(items);
        $("input[type=checkbox]").click(function(){
          if ($("input[type=checkbox]:checked").length == array.multiselect.selectno) {
            $("input[type=checkbox]:not(:checked)").attr("disabled", "disabled");
            $("input[type=checkbox]").checkboxradio("refresh");
          }
          else {
            $("input[type=checkbox]").removeAttr("disabled");
            $("input[type=checkbox]:not(:checked)").removeAttr("title");
            $("input[type=checkbox]").checkboxradio("refresh");
          }
        });
        $("input[type=radio],input[type=checkbox]").click(function () {
          if ($("input[type=radio]:checked").length > 0) {
            $("#polllbutton").button("enable");
            $("#polllbutton").removeAttr("disabled");
            $("#polllbutton").button("refresh");
          }
          else if ($("input[type=checkbox]:checked").length >= array.multiselect.minselect) {
            $("#polllbutton").button("enable");
            $("#polllbutton").button("refresh");
          }
          else {
            $("#polllbutton").button("disable");
            $("#polllbutton").button("refresh");
          }
        });
        $("input[type=checkbox]").checkboxradio();
      }
      else {
        $("#selecttext").html("one:");
        var items = "";
        $.each(array.choices, function(key, value){
          items = items + "<input type='radio' name='vote"+ pollid +"'  id='vote"+ key +"' value='"+ value +"'> <label for='vote"+key+"'>" + value + "</label>";
        });
        $(".items").html(items);
        $("input[type=radio],input[type=checkbox]").click(function () {
          if ($("input[type=radio]:checked").length > 0) {
            $("#polllbutton").button("enable");
            $("#polllbutton").removeAttr("disabled");
            $("#polllbutton").button("refresh");
          }
          else if ($("input[type=checkbox]:checked").length >= array.multiselect.minselect) {
            $("#polllbutton").button("enable");
            $("#polllbutton").button("refresh");
          }
          else {
            $("#polllbutton").button("disable");
            $("#polllbutton").button("refresh");
          }
        });
        $("input[type=radio]").checkboxradio();
      }
      $("#back").bind('tap', function () {
        $("#pollidform").css("display", "inline");
        $("#pollform").css("display", "none");
        $(".choices").css("display", "none");
        $("#pollid").val("");
        $("#pollbutton").val("Submit");
        $("#pollbutton").button('refresh');
        $("h2").text("Enter Poll ID");
        $("#instructions").html("");
        $("input").removeAttr("disabled");
        $("#polllbutton").val("Vote");
        $("#polllbutton").button("disable");
        $("#polllbutton").button('refresh');
      });
    }
    else if (array.errorcode == "1") {
      $("#pollbutton").button('disable');
      $("#pollbutton").val("Submit");
      $("#pollbutton").button('refresh');
      $("#errormsg").html(array.description + ", please try again.");
      $("#linkerror").click();
    }
    else {
      $("#pollbutton").button('disable');
      $("#pollbutton").val("Submit");
      $("#pollbutton").button('refresh');
      $("#errormsg").html(array.description);
      $("#linkerror").click();
    }
  });
}

function registervote(pollid, votechoice){
  if ($("input[type=checkbox]").length > 0) {
    for (var i = 0; i < votechoice.length; i++) {
      var tempchoice = votechoice[i];
      console.log(pollid + " " + tempchoice);
      $.post("ajax/server.php", {action: "2", pollid: pollid, votechoice: tempchoice}, function (data) {
        console.log(data);
      });
    }
    $("input").attr("disabled", "disabled");
    $("#polllbutton").val("Your vote has been casted");
    $("#polllbutton").button('refresh');
    $("input[type=radio]").checkboxradio('refresh');
  }
  else {
    $.post("ajax/server.php", {action: "2", pollid: pollid, votechoice: votechoice}, function (data) {
      if (data == "1") {
        $("input").attr("disabled", "disabled");
        $("#polllbutton").val("Your vote has been casted");
        $("#polllbutton").button('refresh');
        $("input[type=radio]").checkboxradio('refresh');
      }
      else {
        console.log(data);
      }
    });
  }
}
