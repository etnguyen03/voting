<!-- Ethan Voting
Version 1.1

Copyright 2015 Ethan Nguyen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Voting</title>
    <link rel="stylesheet" href="css/main.css" media="screen" title="no title" charset="utf-8">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.4.js" charset="utf-8"></script>
    <script src="js/mobile.js" charset="utf-8"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.4/jquery-ui.js" charset="utf-8"></script>
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.4/themes/smoothness/jquery-ui.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile.structure-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile.theme-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile-1.4.5.js" charset="utf-8"></script>
  </head>
  <body>
    <div data-role="page">
      <div class="header" data-role="header">
        <h1>Ethan Voting</h1>
      </div>
      <div class="main" data-role="content">
        <h2>Enter Poll ID</h2>
        <form id='pollidform'>
          <input type="number" id="pollid" placeholder='Poll ID' required>
          <input type="submit" id='pollbutton' value="Submit"  data-icon="check">
        </form>
        <div id='jsblurb'>
          <strong><font style='color: red;'>Please enable JavaScript, then reload the page</font></strong>
        </div>
        <p id='instructions'>

        </p>
        <div class="choices">
          <form id="pollform">
            <fieldset data-role="controlgroup">
              <legend>Select <font id='selecttext'></font></legend>
              <div class="items">

              </div>
            </fieldset>
            <br>
            <input type="button" id='polllbutton' value="Vote" data-icon="check">
            <button type="button" id='back' data-icon="back">Back</button>
          </form>
        </div>
        <!-- <div class="hidden"> -->
          <a href="#error" data-rel="popup" id='linkerror' data-position-to="window" class="linkpopup" data-transition="pop">Err</a>
          <div id="error" title='Error' data-role="popup" data-dismissible="false" data-overlay-theme="b">
            <div data-role="header"><h1>Error</h1></div>
            <div role="main" class="ui-content">
              <p id='errormsg'></p>
              <center><a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" data-rel="back">OK</a></center>
            </div>
          </div>
          <a href="#conf" data-rel="popup" id='linkconf' data-position-to="window" class="linkpopup" data-transition="pop">Cnfrm</a>
          <div id='conf' data-role="popup" data-dismissible="false" data-overlay-theme="b">
            <div data-role="header"><h1>Are you sure?</h1></div>
            <div role="main" class="ui-content">
              <p id='confmsg'></p>
              <center><a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" data-rel="back" id="confyesbtn" data-theme="b">Yes</a>
              <a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" data-rel="back">No</a></center>
            </div>
          </div>
          <div id="license" data-role="popup" data-overlay-theme="b">
            <div data-role="header"><h1>License</h1> <a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" data-rel="back">Close</a></div>
            <div role="main" class="ui-content">
              <iframe src="newlicense.php" width="480" height="320" seamless></iframe>
            </div>
          </div>
        <!-- </div> -->
      </div>
      <div class="footer" data-role="footer">
        <h4>Copyright (c) 2015 Ethan Nguyen. All Rights Reserved. <a href="#license" id="licenselink" data-rel="popup" data-position-to="window" data-transition="pop">License</a></h4>
      </div>
    </div>
  </body>
</html>
