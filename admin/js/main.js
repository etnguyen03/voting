$(document).ready(function (event) {
  $("#pollidformid").keypress(function (e) {
    if (e.which == 13) {
      $("#pollidformsubmitbtn").trigger("tap");
      e.preventDefault();
    }
  });
  $("#pollidformsubmitbtn").bind("tap", function (e) {
    var pollid = $("#pollidformid").val();
    if (pollid == "") {
      $("#errordialogmsg").html("Please enter a Poll ID.");
      $("#errorpopuplink").click();
    }
    else {
      $.post("ajax/server.php", {"action": "1", "pollid": pollid}, function (data) {
        var parsed = $.parseJSON(data);
        if (typeof parsed.errorcode == "undefined") {
          // parse the json
          parsed.choices = $.parseJSON(parsed.choices);
          parsed.multiselect = $.parseJSON(parsed.multiselect);

          // hide the poll id form and show the administ.
          $(".pollidform").hide();
          $(".administr").show();

          $(".admindiv").hide();
          $("#adminname").show();

          // change header to title
          $("#pollheadername").html(parsed.title);

          // change name box to title
          $("#adminnamebox").val(parsed.title);

          // change instructions box to instructions
          $("#admindescriptionbox").text(parsed.instructions);

          // add the options
          $.each(parsed.choices, function (index, element) {
            $("#adminoptionslist").html($("#adminoptionslist").html() + "<li id='option"+ index +"' class='option"+ pollid +"'><input type='text' id='optiontextbox"+ index +"' class='optiontextbox optiontextbox"+ pollid +"' value='" + element + "'></li>")
          });

          $(".optiontextbox" + pollid).textinput();
          $("#adminoptionslist").listview("refresh");

          // add poll id to #administrpollid
          $("#administrpollid").html(pollid);

          // set disabled text
          if (parsed.disabled == 1) {
            $("#admindisablestat").html("disabled");
          }
          else {
            $("#admindisablestat").html("enabled");
          }

          // multiselect
          if (parsed.multiselect.enabled == "true") {
            $("#adminoptionsmultiselectstatus").html("enabled");
            $("#adminoptionsmultiselectoptionsmin").val(parsed.multiselect.minselect)
            $("#adminoptionsmultiselectoptionsmax").val(parsed.multiselect.selectno)
            $("#adminoptionsmultiselectoptions").show();
          }
          else {
            $("#adminoptionsmultiselectstatus").html("disabled");
          }

          associateevents();

          // refresh the vote count
          $("#adminvotesrefresh").trigger("tap");
        }
        else {
          // show the error dialog
          $("#errordialogmsg").html(parsed.description);
          $("#errorpopuplink").click();
        }
      });
    }
    e.preventDefault();
  });
  $("#pollidformcreatebtn").bind("tap", function (e) {
    $.post("ajax/server.php", {"action": "9"}, function (data) {
      $("#createdialogmsg").html("Your poll has been created with a Poll ID of " + data + ". Please note that your poll starts off disabled.");
      $("#createpopuplink").click();
      $("#pollidformid").val(data);
      $("#pollidformsubmitbtn").click();
    });
    e.preventDefault();
  });

  $("#navbarname").bind("tap", function () {
    $(".admindiv").hide();
    $("#adminname").show();
  });
  $("#navbardescription").bind("tap", function () {
    $(".admindiv").hide();
    $("#admindescription").show();
  });
  $("#navbarchoices").bind("tap", function () {
    $(".admindiv").hide();
    $("#adminoptions").show();
  });

  $("#navbarvotes").bind("tap", function () {
    $(".admindiv").hide();
    $("#adminvotes").show();
  });

  $("#navbardisable").bind("tap", function () {
    $(".admindiv").hide();
    $("#admindisable").show();
  })

  $("#adminvotesautoupdate").change(function () {
    if ($("#adminvotesautoupdate").is(":checked") == true) {
      window.setTimeout(refreshvotes, 2500);
    }
  });
});

function refreshvotes() {
  $("#adminvotesrefresh").trigger("tap");

  // if autorefresh checked or not
  if ($("#adminvotesautoupdate").is(":checked") == true) {
    window.setTimeout(refreshvotes, 2500);
  }
}



function associateevents(){
  // start refresh timer
  window.setTimeout(refreshvotes, 2500);

  // button to refresh vote count
  $("#adminvotesrefresh").bind("tap", function () {
    var pollid = $("#administrpollid").html();
    $.post("ajax/server.php", {"action": "7", "pollid": pollid}, function (data) {
      var parsed = $.parseJSON(data);
      var html = "";
      var count = 0;
      $.each(parsed, function (i) {
        count = count + parsed[i]["votes"];
      });
      $.each(parsed, function (i) {
        html = html + "<tr>";
        html = html + "<th data-priority='3'>"+ i + "</th>";
        html = html + "<td data-priority='1'>"+ parsed[i]["val"] + "</td>";
        html = html + "<td data-priority='2'>"+ parsed[i]["votes"] + "</td>";
        html = html + "<td data-priority='2'>"+ Math.round((parsed[i]["votes"]/count) * 100) + "%</td>";
        html = html + "</tr>";
      });
      html = html + "<tr><th>TOTAL</th><td></td><td><b>" + count + "</b></td><td><b>100%</b></td></tr>";
      $("#adminvotestablebody").html(html);
    });
  });
  $("#admindisabletoggle").bind("tap", function () {
    var pollid = $("#administrpollid").html();
    $.post("ajax/server.php", {"action": "8", "pollid": pollid}, function (data) {
      if (data == 1) {
        var currentstat = $("#admindisablestat").html();
        if (currentstat == "disabled") {
          $("#admindisablestat").html("enabled");
        }
        else {
          $("#admindisablestat").html("disabled");
        }
      }
      else {
        $("#errordialogmsg").html("An error has occurred. Please refresh the page and try again.");
        $("#errorpopuplink").click();
      }
    });
  });
  $("#adminvotesreset").bind("tap", function () {
    $("#resetpopuplink").click();
  });
  $("#resetyesbtn").bind("tap", function () {
    var pollid = $("#administrpollid").html();
    $.post("ajax/server.php", {"action": "10", "pollid": pollid}, function (data) {
      if (data == 1) {
        $("#adminvotesrefresh").trigger("tap");
        $("#resetdialog").popup("close");
      }
      else {
        $("#resetdialog").popup("close");
        $("#errordialogmsg").html("An error has occurred. Please refresh the page and try again.");
        $("#errorpopuplink").click();
      }
    });
  });
  $("#adminoptionsmultiselecttoggle").bind("tap", function () {
    var pollid = $("#administrpollid").html();
    $.post("ajax/server.php", {"action": "11", "pollid": pollid}, function (data) {
      if (data == 1) {
        if ($("#adminoptionsmultiselectstatus").html() == "disabled") {
          $("#adminoptionsmultiselectstatus").html("enabled");
          $("#adminoptionsmultiselectoptions").show();
        }
        else {
          $("#adminoptionsmultiselectstatus").html("disabled");
          $("#adminoptionsmultiselectoptions").hide();
        }
      }
      else {
        $("#errordialogmsg").html("An error has occurred. Please refresh the page and try again.");
        $("#errorpopuplink").click();
      }
    });
  });

  // when name box changed
  $("#adminnamebox").change(function () {
    var pollid = $("#administrpollid").html();
    var name = $("#adminnamebox").val();

    $.post("ajax/server.php", {"action": "2", "pollid": pollid, "name": name}, function (data) {
      if (data == 1) {
        $("#pollheadername").html(name);
      }
      else {
        $("#errordialogmsg").html("An error has occurred. Please refresh the page and try again.");
        $("#errorpopuplink").click();
      }
    });
  });

  // when description box changed
  $("#admindescriptionbox").change(function () {
    var pollid = $("#administrpollid").html();
    var description = $("#admindescriptionbox").val();

    $.post("ajax/server.php", {"action": "3", "pollid": pollid, "description": description}, function (data) {
      if (data == 1) {
      }
      else {
        $("#errordialogmsg").html("An error has occurred. Please refresh the page and try again.");
        $("#errorpopuplink").click();
      }
    });
  });

  // options text boxes
  // if changing, change in the 'add' below too
  $(".optiontextbox").change(function () {
    var pollid = $("#administrpollid").html();
    var option = $(this).val();
    var id = $(this).attr("id");
    id = id.replace("optiontextbox", "");

    $.post("ajax/server.php", {"action": "4", "pollid": pollid, "value": option, "optid": id}, function (data) {
      if (data == 1) {
      }
      else {
        $("#errordialogmsg").html("An error has occurred. Please refresh the page and try again.");
        $("#errorpopuplink").click();
      }
    });
  });

  $("#adminoptionsbuttonsadd").bind("tap", function () {
    var html = $("#adminoptionslist").html();
    var pollid = $("#administrpollid").html();
    var optid = $(".optiontextbox").last().attr("id");
    optid = optid.replace("optiontextbox", "");
    optid = parseInt(optid) + 1;
    html = html + "<li id='option"+ optid +"' class='option"+ pollid +"'><input type='text' id='optiontextbox"+ optid +"' class='optiontextbox optiontextbox"+ pollid +"' value=''></li>"
    $("#adminoptionslist").html(html);
    $("#adminoptionslist").listview("refresh");
    $("#optiontextbox" + optid).textinput();

    // binding for the 'change' - if changing here change above too
    $(".optiontextbox").change(function () {
      var pollid = $("#administrpollid").html();
      var option = $(this).val();
      var id = $(this).attr("id");
      id = id.replace("optiontextbox", "");

      $.post("ajax/server.php", {"action": "4", "pollid": pollid, "value": option, "optid": id}, function (data) {
        if (data == 1) {
        }
        else {
          $("#errordialogmsg").html("An error has occurred. Please refresh the page and try again.");
          $("#errorpopuplink").click();
        }
      });
    });
  });

  // remove last option button
  $("#adminoptionsbuttonsrem").bind("tap", function () {
    var html = $("#adminoptionslist").html();
    var pollid = $("#administrpollid").html();

    $.post("ajax/server.php", {"action": "6", "pollid": pollid}, function (data) {
      if (data == "1") {
        $(".optiontextbox").last().parent().parent().remove();
        $("#adminoptionslist").listview("refresh");
      }
      else {
        $("#errordialogmsg").html("An error has occurred. Please refresh the page and try again.");
        $("#errorpopuplink").click();
      }
    });
  });
}
