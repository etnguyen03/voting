<?php
  // Ethan Voting
  // Version 1.1
  //
  // Copyright 2015 Ethan Nguyen
  //
  // Licensed under the Apache License, Version 2.0 (the "License");
  // you may not use this file except in compliance with the License.
  // You may obtain a copy of the License at
  //
  //  http://www.apache.org/licenses/LICENSE-2.0
  //
  // Unless required by applicable law or agreed to in writing, software
  // distributed under the License is distributed on an "AS IS" BASIS,
  // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  // See the License for the specific language governing permissions and
  // limitations under the License.

  // DIRECTORY:
  // Action 1: Get poll data, takes pollid
  // Action 2: Set the poll title, takes pollid and name
  // Action 3: Set the poll description, takes pollid and description
  // Action 4: Set choice, takes pollid, optid, and value
  // Action 5: Get number of choices, takes pollid
  // Action 6: Remove one choice, takes pollid
  // Action 7: Get array of votes, takes pollid
  // Action 8: Toggles disable/enable, takes pollid
  // Action 9: Creates a poll with filler data, returns pollid
  // Action 10: Resets votes, takes pollid
  // Action 11: Toggles multiselect, takes pollid
  // Action 12: Sets maximum select, takes pollid and value
  // Action 13: Sets minimum select, takes pollid and value

  $action = $_POST['action'];
  require 'connect.php';
  if ($action == "1") {
    $pollid = $_POST['pollid'];
    $query = $db->query("SELECT * FROM polls WHERE id='$pollid'");
    if ($query->num_rows == 1) {
      echo json_encode($query->fetch_assoc());
    }
    else {
      echo json_encode(array('errorcode' => 1,'description' => 'Invalid poll ID'));
    }
  }
  elseif ($action == "2") {
    $pollid = $_POST['pollid'];
    $name = $_POST['name'];

    $db->query("UPDATE polls SET title='$name' WHERE id='$pollid'");
    echo "1";
  }
  elseif ($action == "3") {
    $pollid = $_POST['pollid'];
    $description = $_POST['description'];

    $db->query("UPDATE polls SET instructions='$description' WHERE id='$pollid'");
    echo "1";
  }
  elseif ($action == "4") {
    $optid = $_POST['optid'];
    $value = $_POST['value'];
    $pollid = $_POST['pollid'];

    $query = $db->query("SELECT choices FROM polls WHERE id='$pollid'");
    if ($query->num_rows == 1) {
      $choices = json_decode($query->fetch_assoc()['choices'], true);
      $choices[$optid] = $value;
      $choices = json_encode($choices);
      $db->query("UPDATE polls SET choices='$choices' WHERE id='$pollid'");
      echo "1";
    }
    else {
      throw new Exception("Poll ID not found", 1);
    }
  }
  elseif ($action == "5") {
    $pollid = $_POST['pollid'];
    $query = $db->query("SELECT choices FROM polls WHERE id='$pollid'");
    if ($query->num_rows == 1) {
      $choices = json_decode($query->fetch_assoc()['choices'], true);
      echo count($choices);
    }
    else {
      throw new Exception("Poll ID not found", 1);
    }
  }
  elseif ($action == "6") {
    $pollid = $_POST['pollid'];

    $query = $db->query("SELECT choices FROM polls WHERE id='$pollid'");
    if ($query->num_rows == 1) {
      $choices = json_decode($query->fetch_assoc()['choices'], true);

      array_pop($choices);
      $choices = json_encode($choices);
      $db->query("UPDATE polls SET choices='$choices' WHERE id='$pollid'");
      echo "1";
    }
    else {
      throw new Exception("Poll ID not found", 1);

    }
  }
  elseif ($action == "7") {
    $pollid = $_POST['pollid'];

    $query = $db->query("SELECT choices FROM polls WHERE id='$pollid'");
    if ($query->num_rows == 1) {
      $choices = json_decode($query->fetch_assoc()['choices'], true);
      $query = $db->query("SELECT * FROM votes WHERE pollid='$pollid'");
      $return = array();
      if ($query->num_rows > 0) {
        foreach ($choices as $key => $value) {
          $query = $db->query("SELECT * FROM votes WHERE pollid='$pollid' AND choice='$key'");
          $return[$key] = array("val" => "$value", "votes" => $query->num_rows);
        }
      }
      else {
        foreach ($choices as $key => $value) {
          $return[$key] = array("val" => "$value", "votes" => 0);
        }
      }
      echo json_encode($return);
    }
    else {
      throw new Exception("Poll ID not found", 1);
    }
  }
  elseif ($action == "8") {
    $pollid = $_POST['pollid'];
    $query = $db->query("SELECT disabled FROM polls WHERE id='$pollid'");
    if ($query->num_rows == 1) {
      $disabled = $query->fetch_assoc()['disabled'];
      if ($disabled == 0) {
        $disabled = 1;
      }
      else {
        $disabled = 0;
      }
      $db->query("UPDATE polls SET disabled='$disabled' WHERE id='$pollid'");
      echo "1";
    }
  }
  elseif ($action == "9") {
    $choices = json_encode(array("1" => "choice".uniqid()));
    $title = "Poll " . uniqid();
    $instructions = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
    // $multiselect = '{"enabled": "false","selectno": "null", "minselect": "null"}';
    $multiselect = json_encode(array("enabled" => "false", "selectno" => "null", "minselect" => "null"));
    $multiselect = $db->real_escape_string($multiselect);
    $db->query("INSERT INTO polls (choices, instructions, title, multiselect, disabled) VALUES ('$choices', '$instructions', '$title', '$multiselect', 1)");
    $query = $db->query("SELECT * FROM polls WHERE title='$title' AND instructions='$instructions' AND choices='$choices'");
    if ($query->num_rows == 1) {
      echo $query->fetch_assoc()['id'];
    }
    else {
      throw new Exception("Poll ID not found", 1);

    }
  }
  elseif ($action == "10") {
    $pollid = $_POST['pollid'];

    $db->query("DELETE FROM votes WHERE pollid='$pollid'");
    echo "1";
  }
  elseif ($action == "11") {
    $pollid = $_POST['pollid'];
    $query = $db->query("SELECT * FROM polls WHERE id='$pollid'");
    if ($query->num_rows == 1) {
      $multiselect = $query->fetch_assoc()['multiselect'];
      $multiselect = json_decode($multiselect, true);
      if ($multiselect['enabled'] == "false") {
        $multiselect['enabled'] = "true";
      }
      else {
        $multiselect['enabled'] = "false";
      }
      $multiselect = json_encode($multiselect);
      $db->query("UPDATE polls SET multiselect='$multiselect' WHERE id='$pollid'");
      echo "1";
    }
    else {
      throw new Exception("Poll ID not found", 1);
    }
  }
  elseif ($action == "12") {
    $pollid = $_POST['pollid'];
    $value = $_POST['value'];

    $query = $db->query("SELECT * FROM polls WHERE id='$pollid'");
    if ($query->num_rows == 1) {
      $multiselect = $query->fetch_assoc()['multiselect'];
      $multiselect = json_decode($multiselect, true);
      $multiselect['selectno'] = $value;
      $multiselect = json_encode($multiselect);
      $db->query("UPDATE polls SET multiselect='$multiselect' WHERE id='$pollid'");
      echo "1";
    }
    else {
      throw new Exception("Poll ID not found", 1);

    }

  }
  elseif ($action == "13") {
    $pollid = $_POST['pollid'];
    $value = $_POST['value'];

    $query = $db->query("SELECT * FROM polls WHERE id='$pollid'");
    if ($query->num_rows == 1) {
      $multiselect = $query->fetch_assoc()['multiselect'];
      $multiselect = json_decode($multiselect, true);
      $multiselect['minselect'] = $value;
      $multiselect = json_encode($multiselect);
      $db->query("UPDATE polls SET multiselect='$multiselect' WHERE id='$pollid'");
      echo "1";
    }
    else {
      throw new Exception("Poll ID not found", "1");
    }
  }
 ?>
