<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <script src="js/main.js" charset="utf-8"></script>
    <link rel="stylesheet" href="css/main.css" media="screen" title="no title">
    <title>Ethan Voting - Administrator</title>
  </head>
  <body>
    <div data-role="page" id="mainpage">
      <div data-role="header" class="ui-title">
        <h1>Ethan Voting - Administrator</h1>
      </div>
      <div data-role="content">
        <h2 id='pollheadername'>Enter Poll ID</h2>
        <form class="pollidform">
          <input type="number" id="pollidformid" value="" placeholder="Poll ID" />
          <button class="ui-shadow ui-btn ui-corner-all ui-btn-icon-left ui-icon-check" id="pollidformsubmitbtn">Submit</button>
          <button class="ui-shadow ui-btn ui-corner-all ui-btn-icon-left ui-icon-edit" id="pollidformcreatebtn">Create</button>
        </form>
        <div class="administr">
          <p>
            Poll ID: <font id="administrpollid"></font>
          </p>
          <div data-role="navbar">
            <ul>
              <li><a href="#" class="ui-btn-active ui-state-persist" id="navbarname">Name</a></li>
              <li><a href="#" class="ui-btn" id="navbardescription">Description</a></li>
              <li><a href="#" class="ui-btn" id="navbarchoices">Choices</a></li>
              <li><a href="#" class="ui-btn" id="navbarvotes">Votes</a></li>
              <li><a href="#" class="ui-btn" id="navbardisable">Enable/Disable</a></li>
            </ul>
          </div>
          <br />
          <div id="adminname" class="admindiv">
            <input type="text" id="adminnamebox" value="">
          </div>
          <div id="admindescription" class="admindiv">
            <textarea id="admindescriptionbox" style="height: 400px;"></textarea>
          </div>
          <div id="adminoptions" class="admindiv">
            <ul id="adminoptionslist" data-role="listview">

            </ul>
            <br />
            <div id="adminoptionsbuttons">
              <button id="adminoptionsbuttonsadd" data-icon="plus">Add More</button>
              <button id="adminoptionsbuttonsrem" data-icon="delete">Remove Last Option</button>
            </div>
            <div id="adminoptionsmultiselect">
              <h3>Multiselect</h3>
              <p>
                Multiselect is <font id="adminoptionsmultiselectstatus"></font>.
              </p>
              <button id="adminoptionsmultiselecttoggle" data-icon="recycle">Toggle</button>
              <div id="adminoptionsmultiselectoptions">
                <input type="text" id="adminoptionsmultiselectoptionsmin" placeholder="Minimum Select"/>
                <input type="text" id="adminoptionsmultiselectoptionsmax" placeholder="Maximum Select"/>
              </div>
            </div>
          </div>
          <div id="adminvotes" class="admindiv">
            <fieldset>
              <div data-role="fieldcontain">
                <label for="adminvotesautoupdate">Automatically update:</label>
                <input type="checkbox" id="adminvotesautoupdate" data-role="flipswitch" checked />
              </div>
            </fieldset>
            <table data-role="table" data-mode="columntoggle" id="adminvotestable">
              <thead>
                <tr>
                  <th data-priority="3" data-colstart="1" class="ui-table-cell-visible">ID</th>
                  <th data-priority="1" data-colstart="2" class="ui-table-cell-visible">Option</th>
                  <th data-priority="2" data-colstart="3" class="ui-table-cell-visible">Votes</th>
                  <th data-priority="3" data-colstart="4" class="ui-table-cell-visible">Percentage</th>
                </tr>
              </thead>
              <tbody id="adminvotestablebody">

              </tbody>
            </table>
            <button id="adminvotesrefresh" data-icon="refresh">Refresh Votes</button>
            <button id="adminvotesreset" data-icon="recycle">Reset Votes</button>
          </div>
          <div class="admindiv" id="admindisable">
            <p>
              This poll is currently <font id="admindisablestat"></font>.
            </p>
            <button id="admindisabletoggle" data-icon="recycle">Toggle</button>
          </div>
        </div>
      </div>
      <div data-role="footer" class="ui-title">
        <h4>Copyright (c) 2015 Ethan Nguyen. All Rights Reserved. <a href="#license" id="licenselink" data-rel="popup" data-position-to="window" data-transition="pop">License</a></h4>
      </div>
      <div class="hidden">
        <a href="#errordialog" id="errorpopuplink" data-rel="popup" data-position-to="window" data-transition="pop">Err</a>
        <a href="#createdialog" id="createpopuplink" data-rel="popup" data-position-to="window" data-transition="pop">Create</a>
        <a href="#resetdialog" id="resetpopuplink" data-rel="popup" data-position-to="window" data-transition="pop">Reset</a>
      </div>
      <div id="errordialog" data-role="popup" data-dismissible="false" data-overlay-theme="b">
        <div data-role="header"><h1>Error</h1></div>
        <div role="main" class="ui-content">
          <p id='errordialogmsg'></p>
          <center><a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" data-rel="back">OK</a></center>
        </div>
      </div>
      <div id="createdialog" data-role="popup" data-dismissible="false" data-overlay-theme="b">
        <div data-role="header"><h1>Poll Created</h1></div>
        <div role="main" class="ui-content">
          <p id='createdialogmsg'></p>
          <center><a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" data-rel="back">OK</a></center>
        </div>
      </div>
      <div id="resetdialog" data-role="popup" data-dismissible="false" data-overlay-theme="b">
        <div data-role="header"><h1>Are you sure?</h1></div>
        <div role="main" class="ui-content">
          <p id='resetdialogmsg'>Are you sure you want to reset the vote count? This will allow devices to vote again.</p>
          <center><a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" id="resetyesbtn">Yes</a><a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" data-rel="back">No</a></center>
        </div>
      </div>
      <div id="license" data-role="popup" data-overlay-theme="b">
        <div data-role="header"><h1>License</h1> <a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" data-rel="back">Close</a></div>
        <div role="main" class="ui-content">
          <iframe src="newlicense.php" width="480" height="320" seamless></iframe>
        </div>
      </div>
    </div>
  </body>
</html>
