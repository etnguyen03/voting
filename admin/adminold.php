<!-- Ethan Voting
Version 1.1

Copyright 2015 Ethan Nguyen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Voting - Administrator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile.structure-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile.theme-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile-1.4.5.js" charset="utf-8"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.4.js" charset="utf-8"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.4/jquery-ui.js" charset="utf-8"></script>
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.4/themes/smoothness/jquery-ui.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="css/mainold.css" media="screen" title="no title" charset="utf-8">
    <script src="js/mainold.js" charset="utf-8"></script>
  </head>
  <body>
    <div data-role="page">
      <div data-role="header">
        <h1>Ethan Voting - Administrator</h1>
      </div>
      <div role="main" class="ui-content">
        <div class="jsblurb">
          <strong><font style='color: red;'>Please enable JavaScript, then reload the page</font></strong>
        </div>
        <div class="pollidform">
          <form class="pollidform">
            <input type="text" id='pollid' placeholder="Poll ID" required>
            <input type="submit" id='pollidb' value="Submit" class="ui-btn">
          </form>
          <div style='padding-top: 10px;'>
            <button type="button" id='create' class="ui-btn">Create</button>
          </div>
        </div>
        <h2 class='polltitle'></h2>
        <div class="administr">
          <h3>Poll ID (setup for voters)</h3>
          <div>
            <strong align="center" class='ui-widget-header header'>Please go to this website:</strong><br>
            <span class='header-bigger'><input type='text' placeholder='Path to access client voting' style='font-size: 4em; width: 90%; outline: none; '></span><br>
            <strong align="center" class='ui-widget-header header'>The Poll ID is:</strong><br>
            <span class='pollidsetup'></span>
          </div>
          <h3>Name</h3>
          <div>
            <input type="text" id='namebox' value="" placeholder="Name" title='Autosaves' class='tooltip' maxlength="250">
          </div>
          <h3>Instructions</h3>
          <div>
            <!-- <input type="text" id='descriptionbox' value="" placeholder="Description" title='Autosaves' class='tooltip' maxlength="1000"> -->
            <textarea id='descriptionbox' rows="8" cols="40" placeholder="Instructions" title="Autosaves" class="tooltip" maxlength="1000"></textarea>
          </div>
          <h3>Options</h3>
          <div>
            <ul id='optionslist'>

            </ul>
          </div>
          <h3>Votes</h3>
          <div>
            <table id='votetable' class='ui-widget'>
              <tr class='ui-widget-header'>
                <th>
                  Option Number
                </th>
                <th>
                  Option Text
                </th>
                <th>
                  Votes
                </th>
              </tr>
            </table>
            <div style='padding-top: 10px;'>
              <button type="button" id='refresh'>Refresh Stats</button>
              <button type="button" id='deletevotes'>Reset Votes</button>
            </div>
          </div>
          <h3>Disabling</h3>
          <div>
            This poll is <strong id='disable'></strong>.
            <button type="button" id="disabler">Toggle</button>
          </div>
          <h3>Multiselect</h3>
          <div>
            Multiselect is <strong id='multiselectstat'></strong>.
            <button type="button" id='multiselectb'>Toggle</button>
            <div style='display: none;' id='multiselectdiv' style='padding-top: 5px;'>
              <table style='border: none;'>
                <tr>
                  <td style='border:none;'>
                    Minimum Select:
                  </td>
                  <td style='border:none;'>
                    <input type="number" id="minno" title="Autosaves" value="">
                  </td>
                </tr>
                <tr>
                  <td style='border: none;'>
                    Maximum Select:
                  </td>
                  <td style='border: none;'>
                    <input type="number" id='maxno' title='Autosaves' value="">
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <br>
        <a href='#' id='blink'>< Back</a>
        <div class="hidden">
          <div id='errorD' title='Error'>
            <p>

            </p>
          </div>
          <div id='newD' title='Poll Created'>
            <p>

            </p>
          </div>
        </div>
      </div>
      <div data-role="footer" class="footer">
        Copyright (c) 2015 Ethan Nguyen. All Rights Reserved.
      </div>
    </div>
  </body>
</html>
