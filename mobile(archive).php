<!-- Ethan Voting
Version 1.1

Copyright 2015 Ethan Nguyen

**** Do not use this file - it's out of date ****

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Voting</title>
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.4.js" charset="utf-8"></script>
    <script src="js/mobile.js" charset="utf-8"></script>
    <link rel="stylesheet" href="css/mobile.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile.structure-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile.theme-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile-1.4.5.js" charset="utf-8"></script>
  </head>
  <body>
    <h1>Ethan Voting</h1>
    <h2>Please enter your Poll ID</h2>
    <div class="pollidform">
      <input type="number" name="number" id='pollid' value="" placeholder="Poll ID">
      <input type="button" id="pollidbutton" value="Submit">
    </div>
    <div class="pollform">
      <p id="instructions">

      </p>
      <div class="optionsspan"></div>
      <input type="button" id="polllbutton" value="Submit">
    </div>
    <div class="hidden">
      <a href="#errorD" id='clickerrorD'></a>
      <div id="errorD" style='max-width: 400px;' data-role="popup" data-theme="b" data-overlay-theme="b" data-dismissible="false">
        <p>

        </p>
      </div>
      <div id='conf' style='max-width: 400px;' data-role ="popup" data-theme="b" data-overlay-theme="b" data-dismissible="false">
        <p>

        </p>
      </div>
    </div>
  </body>
</html>
